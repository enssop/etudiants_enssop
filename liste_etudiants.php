<?php

include "config.php";
include "nav.html";

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Liste des étudiants</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Accueil</a></li>
              <li class="breadcrumb-item active">Etudiants</li>
            </ol>
          </div><!-- /.col -->   
        </div><!-- /.row -->
        <div><a href="ajout_etudiant.php" style="background-color:#00bd49; color:white; text-align:center; padding:5px 50px; font-size:20px; border-radius:5px;">Ajouter un étudiant</a></div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

          <?php
          $req = $bdd->prepare("SELECT * FROM etudiant");
          $req->execute();
          $results = $req->fetchALL();
          foreach ($results as $etudiant) {
          ?>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $etudiant["prenom_etudiant"];?></h3>

                <p>Lorem Ipsum</p>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a href="modifier_etudiant.php?id=<?php echo $etudiant["id_etudiant"] ?>" class="small-box-footer">Modifier <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div> 
          <?php }?>

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<?php

include "footer.html";

?>