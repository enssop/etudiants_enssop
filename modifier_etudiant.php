<?php

include "config.php";

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$recupPrenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? $_POST['prenom']: "";
$recupTelephone = isset($_POST['telephone']) && !empty($_POST['telephone']) ? $_POST['telephone']: "";
$recupEmail = isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email']: "";

$recupIdEtudiant = isset($_GET["id"])?$_GET["id"] : "";

$supIntervention = isset($_GET["sup"])?$_GET["sup"] : "";

if ($supIntervention == 'ok') {
        $req = $bdd->prepare("DELETE FROM etudiant WHERE id_etudiant = ?" );
        $req->execute([$recupIdEtudiant]); 
        header("Location: liste_etudiants.php");
}

$req = $bdd->prepare("SELECT * FROM etudiant
                         WHERE id_etudiant = ?
                         ");
    $req->execute([$recupIdEtudiant]);
    $results = $req->fetchALL();
    $stockInfos = $results[0];

if (isset($_POST['submit'])){
    if (isset($_POST['nom']) && !empty($_POST['nom']) 
    && isset($_POST['prenom']) && !empty($_POST['prenom'])  
    && isset($_POST['telephone']) && !empty($_POST['telephone']) 
    && isset($_POST['email']) && !empty($_POST['email']) 
    )   {
        
        $req = $bdd->prepare("UPDATE etudiant SET nom_etudiant=?, prenom_etudiant=?, telephone_etudiant=?, mail_etudiant=? WHERE id_etudiant=?"); 
        $req->execute([$recupNom, $recupPrenom, $recupTelephone, $recupEmail, $recupIdEtudiant]);
        header("Location: liste_etudiants.php");
            
        }
}

include "nav.html";

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Modification d'un étudiant</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Accueil</a></li>
              <li class="breadcrumb-item active">Modification d'un étudiant</li>
            </ol>
          </div><!-- /.col -->   
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <input type="hidden" name ="nom" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $stockInfos['id_etudiant']?>">
                  </div>
                  <div class="form-group">
                    <label for="nomEtudiant">Nom</label>
                    <input type="text" name ="nom" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $stockInfos['nom_etudiant']?>">
                  </div>
                  <div class="form-group">
                    <label for="prenomEtudiant">Prénom</label>
                    <input type="text" name="prenom" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $stockInfos['prenom_etudiant']?>">
                  </div>
                  <div class="form-group">
                    <label for="telephoneEtudiant">Téléphone</label>
                    <input type="tel" name="telephone" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $stockInfos['telephone_etudiant']?>">
                  </div>
                  <div class="form-group">
                    <label for="emailEtudiant">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $stockInfos['mail_etudiant']?>">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Modifier</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <div><a href="?sup=ok&id=<?php echo $stockInfos["id_etudiant"] ?>" style="background-color:#007bff; color:white; text-align:center; padding:5px 50px; font-size:20px; border-radius:5px;">Supprimer cet étudiant</a></div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<?php

include "footer.html";

?>