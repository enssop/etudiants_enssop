<?php

include "config.php";

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$recupPrenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? $_POST['prenom']: "";
$recupTelephone = isset($_POST['telephone']) && !empty($_POST['telephone']) ? $_POST['telephone']: "";
$recupEmail = isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email']: "";

if (isset($_POST['submit'])){
    if (isset($_POST['nom']) && !empty($_POST['nom']) 
    && isset($_POST['prenom']) && !empty($_POST['prenom'])  
    && isset($_POST['telephone']) && !empty($_POST['telephone']) 
    && isset($_POST['email']) && !empty($_POST['email']) 
    )   {
        
        $req = $bdd->prepare("INSERT INTO etudiant (nom_etudiant, prenom_etudiant, telephone_etudiant, mail_etudiant) VALUES (?,?,?,?)"); 
        $req->execute([$recupNom, $recupPrenom, $recupTelephone, $recupEmail]);
        header("Location: liste_etudiants.php");
            
        }
}

include "nav.html";

?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Ajout d'un étudiant</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Accueil</a></li>
              <li class="breadcrumb-item active">Ajout d'un étudiant</li>
            </ol>
          </div><!-- /.col -->   
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="nomEtudiant">Nom</label>
                    <input type="text" name ="nom" class="form-control" id="exampleInputEmail1" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="prenomEtudiant">Prénom</label>
                    <input type="text" name="prenom" class="form-control" id="exampleInputEmail1" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="telephoneEtudiant">Téléphone</label>
                    <input type="tel" name="telephone" class="form-control" id="exampleInputEmail1" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="emailEtudiant">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Ajouter</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<?php

include "footer.html";

?>